<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    @Auth
        @if (Auth::user()->role->id == 1)
            <!-- Divider -->
        <hr class="sidebar-divider">
        <li class="nav-item {{ Request::is('barang') ? 'active' : '' }}">
            <a class="nav-link" href="/barang">
                <i class="fas fa-fw fa-box"></i>
                <span>Data Barang</span></a>
        </li>
        <hr class="sidebar-divider">
        <li class="nav-item {{ Request::is('barang') ? 'active' : '' }}">
            <a class="nav-link" href="/laporan">
                <i class="fas fa-fw fa-box"></i>
                <span>Laporan Transaksi</span></a>
        </li>
        {{-- <hr class="sidebar-divider">
        <li class="nav-item {{ Request::is('user') ? 'active' : '' }}">
            <a class="nav-link" href="/user">
                <i class="fas fa-fw fa-user"></i>
                <span>Kelola User</span></a>
        </li>
        <li class="nav-item {{ Request::is('role') ? 'active' : '' }}">
            <a class="nav-link " href="/role">
                <i class="fas fa-fw fa-user-tie"></i>
                <span>Kelola Role</span></a>
        </li> --}}
        @endif
        @if (Auth::user()->role->id == 2)
        <hr class="sidebar-divider">
        <li class="nav-item {{ Request::is('history') ? 'active' : '' }}">
            <a class="nav-link" href="/history">
                <i class="fas fa-fw fa-box"></i>
                <span>Riwayat Pembelian</span></a>
        </li>
        @endif
    @endauth
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-box"></i>
            <span>Testing Sidebar</span></a>
    </li>

    <hr class="sidebar-divider">
    <li class="nav-item">
        @if (Auth::user())
            <form action="/logout" method="post">
                @csrf
                <button type="submit" class="nav-link px-3 bg-dark border-0"><i
                        class="fas fa-fw fa-door-open"></i>Logout
                    <span data-feather="log-out"></span></button>
            </form>
        @else
    <li class="nav-item">
        <a class="nav-link " href="/login">
            <i class="fas fa-fw fa-door-closed"></i>
            <span>Login</span></a>
    </li>
    @endif

    </li>


</ul>
<!-- End of Sidebar -->
