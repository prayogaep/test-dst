@extends('layouts.main')

@section('container')
    <h1>Data Transaksi Anda</h1>
    @if (session()->has('status'))
        <div class="alert alert-success col-lg-8" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <table class="table table-striped" id="example1">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga Barang</th>
                <th scope="col">Jumlah Pembelian</th>
                <th scope="col">Total Harga</th>
                <th scope="col">Pembeli</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($pembelian as $p)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $p->barang->nama_barang }}</td>
                    <td>{{ $p->barang->harga_barang }}</td>
                    <td>{{ $p->jumlah_barang }}</td>
                    <td>{{ $p->total_harga }}</td>
                    <td>{{ $p->user->name }}</td>
                </tr>
            @endforeach

    </table>
@endsection

@push('data-tables')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endpush
