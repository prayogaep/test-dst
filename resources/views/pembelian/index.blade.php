@extends('layouts.main')

@section('container')
    <h1>Data Transaksi Anda</h1>

    @if (session()->has('status'))
        <div class="alert alert-success col-lg-8" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga Barang</th>
                <th scope="col">Jumlah Pembelian</th>
                <th scope="col">Total Harga</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pembelian as $p)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $p->barang->nama_barang }}</td>
                    <td>{{ $p->barang->harga_barang }}</td>
                    <td>{{ $p->jumlah_barang }}</td>
                    <td>{{ $p->total_harga }}</td>
                </tr>
            @endforeach

    </table>
@endsection
