@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12">
                <h1>Laravel E - Commerce</h1>
            </div>
            @if (session()->has('status'))
                <div class="alert alert-success col-lg-8" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-8">
                    @foreach ($barang as $b)
                        <div class="card" style="width: 18rem;">
                            @if ($b->image)
                                <img src="{{ asset('storage/' . $b->image) }}" class="card-img-top" alt="...">
                            @else
                                <h5 class="img-thumbnail p-5">Gambar Tidak Tersedia</h5>
                            @endif
                            <div class="card-body">
                                <h5 class="card-title">Nama Barang :{{ $b->nama_barang }}</h5>
                                <h5 class="card-title">Harga : {{ $b->harga_barang }}</h5>
                                <h5 class="card-title">Stok: {{ $b->stok_barang }}</h5>
                                <a href="/transaksi/{{ $b->id }}" class="btn btn-primary">Beli Produk</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
