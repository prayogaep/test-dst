@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="col-lg-8">
            <h2>Form Tambah Data Barang</h2>
            <form action="/transaksi" method="post">
                @csrf
                <div class="mb-3">
                    <label for="barang_id" class="form-label">Nama Barang</label>
                    <input type="hidden" name="barang_id" value="{{ $barang->id }}">
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    <input type="text" class="form-control @error('nama_barang') is-invalid @enderror" id="nama_barang"
                        name="nama_barang" value="{{ $barang->nama_barang }}" readonly>
                    @error('nama_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="harga_barang" class="form-label">Harga Barang</label>
                    <input type="number" min="0" class="form-control @error('harga_barang') is-invalid @enderror"
                        id="harga_barang" name="harga_barang" value="{{ $barang->harga_barang }}"
                        readonly>
                    @error('harga_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="stok_barang" class="form-label">Stok Barang</label>
                    <input type="number" min="0" class="form-control @error('stok_barang') is-invalid @enderror"
                        id="stok_barang" name="stok_barang" value="{{ $barang->stok_barang }}"
                        readonly>
                    @error('stok_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="jumlah_barang" class="form-label">Jumlah Pembelian</label>
                    <input type="number" min="0" class="form-control @error('jumlah_barang') is-invalid @enderror"
                        id="jumlah_barang" name="jumlah_barang">
                    @error('jumlah_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="total_harga" class="form-label">Total Harga</label>
                    <input type="number" min="0" class="form-control @error('total_harga') is-invalid @enderror"
                        id="total_harga" name="total_harga" readonly>
                    @error('total_harga')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Beli Barang</button>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#jumlah_barang, #harga_barang").keyup(function() {
                var harga = $("#harga_barang").val();
                var jumlah = $("#jumlah_barang").val();

                var total = parseInt(harga) * parseInt(jumlah);
                $("#total_harga").val(total);
            });
        });
    </script>
@endsection
