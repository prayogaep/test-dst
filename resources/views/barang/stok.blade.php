@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="col-lg-8">
            <h2>Form Tambah Data Barang</h2>
            <form method="post" action="/stok/{{ $barang->id }}" enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="mb-3">
                    <label for="nama_barang" class="form-label">Nama Barang</label>
                    <input type="text" class="form-control @error('nama_barang') is-invalid @enderror" id="nama_barang"
                        name="nama_barang" value="{{ old('nama_barang', $barang->nama_barang) }}" readonly>
                    @error('nama_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="harga_barang" class="form-label">Harga Barang</label>
                    <input type="number" min="0" class="form-control @error('harga_barang') is-invalid @enderror"
                        id="harga_barang" name="harga_barang" value="{{ old('harga_barang', $barang->harga_barang) }}" readonly>
                    @error('harga_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="jumlah_barang" class="form-label">Stok Barang</label>
                    <input type="number" min="0" class="form-control @error('jumlah_barang') is-invalid @enderror"
                        id="jumlah_barang" name="jumlah_barang" value="{{ old('jumlah_barang', $barang->stok_barang) }}" readonly>
                    @error('stok_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="stok_barang" class="form-label">Tambah Stok Barang</label>
                    <input type="number" min="0" class="form-control @error('stok_barang') is-invalid @enderror"
                        id="stok_barang" name="stok_barang" value="0">
                    @error('stok_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
