@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="col-lg-8">
            <h2>Form Tambah Data Barang</h2>
            <form method="post" action="/barang/{{ $barang->id }}" enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="mb-3">
                    <label for="nama_barang" class="form-label">Nama Barang</label>
                    <input type="text" class="form-control @error('nama_barang') is-invalid @enderror" id="nama_barang"
                        name="nama_barang" value="{{ old('nama_barang', $barang->nama_barang) }}">
                    @error('nama_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="harga_barang" class="form-label">Harga Barang</label>
                    <input type="number" min="0" class="form-control @error('harga_barang') is-invalid @enderror"
                        id="harga_barang" name="harga_barang" value="{{ old('harga_barang', $barang->harga_barang) }}">
                    @error('harga_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="stok_barang" class="form-label">Stok Barang</label>
                    <input type="number" min="0" class="form-control @error('stok_barang') is-invalid @enderror"
                        id="stok_barang" name="stok_barang" value="{{ old('stok_barang', $barang->stok_barang) }}" readonly>
                    @error('stok_barang')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="image" class="form-label">Foto Barang</label>
                    @if ($barang->image)
                        <img src="{{ asset('storage/' . $barang->image) }}" class="img-preview img-fluid mb-3 col-sm-5 d-block">
                    @else
                        <img class="img-preview img-fluid mb-3 col-sm-5">
                    @endif
                    <input class="form-control" onchange="previewImage()" type="file" id="image" name="image">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <script>
        function previewImage() {
            const image = document.querySelector('#image');
            const imgPreview = document.querySelector('.img-preview');

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent) {
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection
