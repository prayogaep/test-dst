@extends('layouts.main')

@section('container')
    <h1>Data Barang</h1>
    <a href="/barang/create" class="btn btn-primary mb-3"><i class="fas fa-solid fa-plus"></i> Tambah Data Barang</a>

    @if (session()->has('status'))
        <div class="alert alert-success col-lg-8" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga Barang</th>
                <th scope="col">Stok Barang</th>
                <th scope="col">Gambar Barang</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($barang as $b)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $b->nama_barang }}</td>
                    <td>{{ $b->harga_barang }}</td>
                    <td>{{ $b->stok_barang }}</td>
                    @if($b->image != null)
                        <td><img src="{{ asset('storage/' . $b->image) }}" width="100px" height="100px"></td>
                    @else
                        <td>Belum ada foto barang</td>
                    @endif
                    
                    <td>
                        <a href="/barang/{{ $b->id }}/edit" class="btn btn-warning mb-2">Edit</a>
                        <a href="/barang/{{ $b->id }}/stok" class="btn btn-warning mb-2">Tambah Stok</a>
                        <form action="/barang/{{ $b->id }}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach

    </table>
@endsection
