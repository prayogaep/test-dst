<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\PembelianController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\StokController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AdminController::class, 'index']);
Route::post('/transaksi', [AdminController::class, 'store'])->middleware('auth');
Route::get('/transaksi/{barang}', [AdminController::class, 'show'])->middleware('auth');

Route::get('/history', [PembelianController::class, 'index'])->middleware('auth');
Route::get('/laporan', [LaporanController::class, 'index'])->middleware('auth');
Route::get('/laporan/export_excel', [LaporanController::class, 'export_excel'])->middleware('auth');


Route::put('/stok/{barang}', [BarangController::class, 'updateStok'])->middleware('auth');
Route::get('/barang/{barang}/stok', [BarangController::class, 'stok'])->middleware('auth');
Route::resource('/barang', BarangController::class)->middleware('auth');

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);
Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);
