<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;
use App\Exports\TransaksiExport;
use Maatwebsite\Excel\Facades\Excel;

class LaporanController extends Controller
{
    public function index()
    {
        $pembelian = Transaksi::all();
        return view('laporan.index', [
            'pembelian' => $pembelian
        ]);
    }

    
    public function export_excel()
	{
		return Excel::download(new TransaksiExport, 'Transaksi.xlsx');
	}
}
