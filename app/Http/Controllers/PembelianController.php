<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembelianController extends Controller
{
    public function index()
    {
        return view('pembelian.index', [
            'pembelian' => Transaksi::where('user_id', auth()->user()->id)->paginate(10)->withQueryString()
        ]);
    }
}
