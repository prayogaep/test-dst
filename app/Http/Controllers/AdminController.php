<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Exports\TransaksiExport;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        return view('dashboard.index', compact('barang'));
    }

    public function show(Barang $barang)
    {
        return view('dashboard.show', [
            'barang' => $barang,
        ]);
    }

    public function store(Request $request)
    {
        

        $simpan = new Transaksi();
        $simpan->barang_id = $request['barang_id'];
        $simpan->user_id = Auth::user()->id;
        $simpan->jumlah_barang = $request['jumlah_barang'];
        $simpan->total_harga = $request['total_harga'];
        $simpan->save();
        // $simpan = new Transaksi();
        // $simpan->barang_id = $validatedData['barang_id'];
        // $simpan->user_id = Auth::user()->id;
        // $simpan->jumlah_barang = $validatedData['jumlah_barang'];
        // $simpan->total_harga = $validatedData['total_harga'];
        // $simpan->save();
        
        return redirect('/history')->with('status', 'Pembelian Berhasil');
    }
}
