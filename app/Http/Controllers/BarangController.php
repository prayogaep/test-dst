<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::orderBy('stok_barang', 'desc')->get();
        return view('barang.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama_barang' => 'required',
            'image' => 'image|file|max:1024',
            'harga_barang' => 'required',
            'stok_barang' => 'required'
        ]);
        if($request->file('image')){
            $validatedData['image'] = $request->file('image')->store('barang-image');
        }

        Barang::create($validatedData);

        return redirect('/barang')->with('status', 'Barang berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        return view('barang.edit', [
            'barang' => $barang
        ]);
    }
    public function stok(Barang $barang)
    {
        return view('barang.stok', [
            'barang' => $barang
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {
        $validatedData = $request->validate([
            'nama_barang' => 'required',
            'image' => 'image|file|max:1024',
            'harga_barang' => 'required',
            'stok_barang' => 'required'
        ]);
        
        if($request->file('image')){
            $validatedData['image'] = $request->file('image')->store('barang-image');
        }
        Barang::where('id', $barang->id)
        ->update($validatedData);

        return redirect('/barang')->with('status', 'Barang berhasil diubah');
    }
    public function updateStok(Request $request, Barang $barang)
    {
        $validatedData = $request->validate([
            'stok_barang' => 'required'
        ]);
         $validatedData['stok_barang'] = $request->stok_barang + $barang->stok_barang; 
        Barang::where('id', $barang->id)
        ->update($validatedData);

        return redirect('/barang')->with('status', 'Stok Berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        Barang::destroy($barang->id);
        return redirect('/barang')->with('status', 'Barang berhasil dihapus');

    }
}
